# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def play
    puts 'Use the tower letters to move the discs.'
    until won?
      from_index, to_index = display_and_read.map(&method(:letter_map))

      unless valid_move?(from_index, to_index)
        puts 'INVALID MOVE'
        next
      end

      move(from_index, to_index)
    end
    puts "\nYOU WIN!!!"
    @towers
  end

  def move(from_tower_index, to_tower_index)
    from_tower = @towers[from_tower_index]
    to_tower = @towers[to_tower_index]

    to_tower.push(from_tower.pop)
  end

  def display_and_read
    render
    prompt
  end

  def prompt
    print 'From: '
    from_tower_char = gets.chomp
    print 'To: '
    to_tower_char = gets.chomp
    [from_tower_char, to_tower_char]
  end

  def render
    puts(2.downto(0).map do |height_index|
      0.upto(2).map do |tower_index|
        (@towers[tower_index][height_index] || ' ').to_s
      end.join(' ' * 3)
    end.join("\n").concat("\n").concat(%w[a b c].join(' ' * 3)))
  end

  def won?
    @towers[1].count == 3 || @towers[2].count == 3
  end

  def valid_move?(from_tower_index, to_tower_index)
    from_tower = @towers[from_tower_index]
    to_tower = @towers[to_tower_index]

    return false if from_tower.empty?
    return true if to_tower.empty?
    from_tower.last < to_tower.last
  end

  def letter_map(char)
    'abc'.index(char)
  end
end
